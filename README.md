# [ES6 - lynda.com](https://www.lynda.com/JavaScript-tutorials/Up-Running-ECMAScript-6/424003-2.html)

## Chapter 01 - What is ECMAScript 6 ?
* ECMA: the European Computer Manufacturer Association
* Transpile: convert ES6 to ES5

## Chapter 02 - Transpiling

### Introduction to Babel
* ES6 code in => ES5 code out

### In-browser Babel transpiling
* Use Babel
    * Get the CDN link for Babel at [cdnjs.com](https://cdnjs.com/)
    * Add the link to the head of document
    `<script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.29/browser.js"></script>`
    * Change `type` attribute to `text/babel`
    ```javascript
    <script type="text/babel">
        ...
    </script>
    ```

### Transpiling ES6 with Babel and babel-node
* Install babel package: `npm install babel`
* Transpile: `babel <input_file> --out-file <output_file>`

### Transpiling with Webpack
* Install webpack: `npm install -g webpack`
* Install babel-loader: `npm install --save-dev babel-loader`
* Create file `webpack.config.js` with following content:
```javascript
module.exports = {
    entry: './script.js',
    output: {filename: 'bundle.js'},
    module: {
        loaders: [
            {test: /\.js?/, loader: 'babel-loader', exclude: /node_modules/}
        ]
    }
};
```
* Run `webpack` command on terminal, output file `bundle.js` will contain the compiled code


## Chapter 03 - ES6 syntax

### Let keyword
* Use to create a variable whose cope is in block

### Const keyword
* Use to create a constant

### Template strings
* Use to format code with variables
* Example ``` `Hello ${name}` ```

### Spread operators
* Syntax: `...<arr_variable>`
* Turn the elements of an array into arguments of a function call, or into elements of an array literal
* Example

```javascript
a = [1, 2, 3];
b = [4, 5];
c = [a, b];
=> c: [[1, 2, 3], [4, 5]]
d = [...a, ...b];
=> d = [1, 2, 3, 4, 5]
```

```javascript
function Max(a, b) {
    return (a > b) ? a : b;
}

var max = Max(...[1, 5]);

```

## Chapter 04 - ES6 Functions & Objects

### Default function parameters
* When we call a function without passing values, it'll use the default values
* If we want to skip any parameter, I'll pass `undefined` to it

### Enhancing object literals

### Arrow functions
* Syntax: `var <func_name> = (<parameters>) => {<code>}`

### Arrow functions and the 'this' scope
* With regular functions, the value of `this` is set based on how the function is called.
* With arrow functions, the value of `this`, the value of `this` inside an arrow function is the same as the value of `this` outside the function.

### Destructuring assignment
* Extract data from arrays and objects and assign them to variables
* Use `[ ]` to destructure array
```javascript
var arr = [1, 2, 3, 4, 5];
var [first,,third] = arr;
```
* Use `{ }` to destructure object
```javascript
var book = {
    title: 'Harry Potter',
    author: 'J. K. Rowling',
    publish_year: 1997,
    num_pages: 500
};
(function({title, publish_year}){
    console.log(`The book ${title} was published in ${publish_year}`);
})(book);
var {title: bookTitle} = book;
console.log(bookTitle);
```

### Generator
* Example
```javascript
function* id_maker(){
  var index = 0;
  while(index < 3)
    yield index++;
}

var gen = id_maker();

console.log(gen.next().value); // gen.next().value = 0, gen.next().done = false
console.log(gen.next().value); // gen.next().value = 1, gen.next().done = false
console.log(gen.next().value); // gen.next().value = 2, gen.next().done = false
console.log(gen.next().value); // gen.next().value = undefined, gen.next().done = true
```

## Chapter 05 - ES6 Classes

### ES6 class syntax
* Example
```javascript
class Vehicle {
    constructor(description, wheels) {
        this.description = description;
        this.wheels = wheels;
    }
    describeYourself() {
        console.log(`I am a ${this.description} with ${this.wheels} wheels`);
    }
}

var bike = new Vehicle("bike", 2);

coolSkiVan.describeYourself();
```
### Class inheritance
* Syntax:
```javascript
class <SubClass> extends <SuperClass> {
    constructor(...) {
        super(...);
    }
}
```
