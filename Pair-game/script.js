const NUM_CELL = 16;
const NUM_CELL_IN_ROW = 4;

//each index refer to an image in directory images
var index = [];
index.length = NUM_CELL;
for (let i = 0; i < index.length; i++) {
    index[i] = Math.floor(i/2) + 1;
}

//shuffle index array
for (let i = index.length; i; i--) {
    let j = Math.floor(Math.random() * i);
    [index[i - 1], index[j]] = [index[j], index[i - 1]];
}

var numOpened = 0; //num of current opened cell
var prev; //previous opened cell
var numPair = 0; //num of true pair

//enable clickability of element witch "func"
function enableClick(element, func) {
    element.onclick = func;
    if (!element.classList.contains("clickable")) {
        element.classList.add("clickable");
    }
}

//disable clickability of element
function disableClick(element) {
    element.onclick = null;
    element.classList.remove("clickable");
}

//hanle click event
function imageClickHanlde() {
    disableClick(this);

    numOpened++;
    let src = this.getAttribute("data");
    this.setAttribute("src", src);

    if (numOpened == 1) {
        prev = this;
    }


    if (numOpened == 2) {
        numOpened = 0;
        if (this.getAttribute("data") != prev.getAttribute("data")) {
            enableClick(this, imageClickHanlde);
            enableClick(prev, imageClickHanlde);
            setTimeout(() => {
                this.setAttribute("src", "images/img_0.jpg");
                prev.setAttribute("src", "images/img_0.jpg");
            },500);
        } else {
            numPair++;
            if (numPair*2 == NUM_CELL) {
                setTimeout(() => alert("You won !"), 200);
            }
        }
    }
}

for(let i = 0; i < NUM_CELL; i++){

    let img = document.createElement("img");
    img.classList.add("box");
    img.classList.add("clickable");
    if (i % NUM_CELL_IN_ROW == 0) {
        img.classList.add("clear-fix");
    }

    img.setAttribute("data", "images/img_" + index[i] + ".jpg");
    img.setAttribute("src", "images/img_0.jpg");
    img.setAttribute("alt", "");

    enableClick(img, imageClickHanlde);

    document.getElementById("main").append(img);
}
